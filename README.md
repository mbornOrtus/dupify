# Dupify

A simple CommandBox module to generate many copies of a file.

Useful for generating large quantities of images/PDFs/documents/etc for testing purposes.

## Installation

1. [Install CommandBox](https://commandbox.ortusbooks.com/setup/installation)
2. Start CommandBox by running `box` in a terminal window
3. Inside CommandBox, run `install dupify --system`

## Usage

Dupify can be run using the following simple format:

```bash
dupify <quantity> <file>
```

Dupify also accepts an optional third argument to specify a destination directory:

```bash
dupify <quantity> <file> [saveTo]
```

### Examples

1. Save 300 copies of my profile photo to the current directory: `dupify 300 profile.jpg`
2. Save 3 resume copies to my desktop: `dupify 3 resume.pdf /home/michael/Desktop`

## HONOR GOES TO GOD ABOVE ALL

Because of His grace, this project exists. If you don't like this, then don't read it, it's not for you.

> "Therefore being justified by **faith**, we have peace with God through our Lord Jesus Christ: By whom also we have access by **faith** into this **grace** wherein we stand, and rejoice in hope of the glory of God." Romans 5:5
