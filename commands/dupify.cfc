/**
* Description of task
*/
component {

	property name="destination" default="./";

	/**
	* Make X copies of file Z
	*/
	function run( required numeric copies, required string file, string destination = variables.destination ) {

		if ( isNull( arguments.copies ) || arguments.copies < 1 ){
			error( "Please specify a positive number of copies to create." );
		}
		if ( isNull( arguments.file ) || arguments.file == "" ){
			error( "Please specify the name of a file to duplicate." );
		}

		var sourceFile = resolvePath( arguments.file );
		var filename = listLast( sourceFile, "/" );
		if ( !fileExists( sourceFile ) ){
			error( "Source file not found. #sourceFile#" );
		}

		print.line( "Generating file copies..." );
		for( var n=0; n < arguments.copies; n++ ){
			var extension = listLast( filename, "." );
			var copyFilename = replace(filename, ".#extension#", "-copy-#n#.#extension#" );
			var destinationFile = "#resolvePath( arguments.destination )#/#copyFilename#";
			fileCopy( sourceFile, destinationFile );
		}
		print.greenLine( "Success! Generated #arguments.copies# copies of #arguments.file#" );
	}

}
